using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Ghost", menuName = "Ghost")]

public class GhostInfo : ScriptableObject
{
    public string ghostWord;
    public string ghostWord_2;

    public int damage;

    public void Initialize()
    {
        if (spawnManager.defineGhost == 0)
        {
            ghostWord = assignWord();
            Debug.Log(ghostWord);
            damage = 5;
        }
        else if (spawnManager.defineGhost == 1)
        {
            ghostWord = assignWord();
            Debug.Log(ghostWord);
            ghostWord_2 = "escudo";
            Debug.Log(ghostWord_2);
            damage = 10;
        }
        else if (spawnManager.defineGhost == 2)
        {
            do
            {
                ghostWord = assignWord();
                Debug.Log(ghostWord);
                ghostWord_2 = assignWord();
                Debug.Log(ghostWord_2);
                damage = 15;
            } while (ghostWord == ghostWord_2);
        }
    }
    public string assignWord()
    {
        string[] wordList = {
        "hola", "casa", "perro", "gato", "mesa", "silla", "pelota", "juego", "agua", "libro",
        "lapiz", "boligrafo", "llave", "caja", "puerta", "ventana", "flor", "arbol", "planta",
        "sol", "luna", "estrella", "cielo", "tierra", "mar", "rio", "nausea", "valle", "campo",
        "ciudad", "calle", "avenida", "parque", "plaza", "centro", "mercado", "tienda", "carro",
        "avion", "barco", "tren", "autobus", "bicicleta", "moto", "zapato", "camisa", "pantalon",
        "vestido", "sombrero", "bufanda"
        };

        int randomWord = Random.Range(0, wordList.Length);

        return wordList[randomWord];
    }
}
