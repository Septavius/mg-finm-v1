using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerType : MonoBehaviour
{
    public GameObject InputFieldPrefab;
    public Canvas canvas;
    private GameObject InputFieldObject;
    private TMP_InputField inputField;
    private bool inputActive = false;


    static public string playerInput;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftAlt))
        {
            if (!inputActive)
            {
                activateInput();
            }
            else
            {
                deactivateInput();
            }
        }

    }

    void activateInput()
    {
        InputFieldObject = Instantiate(InputFieldPrefab, canvas.transform);
        inputField = InputFieldObject.GetComponent<TMP_InputField>();
        inputField.ActivateInputField();
        inputActive = true;
    }

    void deactivateInput()
    {
        Destroy(InputFieldObject);
        playerInput = inputField.text.ToLower();
        Debug.Log(playerInput);
        inputActive = false;

    }
}
