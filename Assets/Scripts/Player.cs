using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    float movV, movH;

    float jumpForce = 5;
    Rigidbody rb;
    bool isGrounded;

    public LayerMask groundMask;

    int speed = 5;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        movH = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        movV = Input.GetAxis("Vertical") * speed * Time.deltaTime;

        transform.Translate(movH, 0, movV);

        isGrounded = Physics.Raycast(transform.position, Vector3.down, 1.5f, groundMask);
    }

    private void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }
    }
}
