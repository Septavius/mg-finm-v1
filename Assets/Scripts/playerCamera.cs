using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerCamera : MonoBehaviour
{
    private GameObject Player;
    float mouseY, mouseX;
    float cameraVerticalR;
    // Start is called before the first frame update
    void Start()
    {
        Player = this.transform.parent.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if((winOrDefeat.isPaused == false) || (pillarScript.isDefeated == false) || (Timer.gameWon == false))
        {
            mouseX = Input.GetAxisRaw("Mouse X");
            mouseY = Input.GetAxisRaw("Mouse Y");

            //Rotaci�n en eje X - Funciona con valores invertidos.

            cameraVerticalR -= mouseY;

            cameraVerticalR = Mathf.Clamp(cameraVerticalR, -90f, 90f);

            transform.localEulerAngles = Vector3.right * cameraVerticalR;

            // Rotaci�n en eje Y

            Player.transform.Rotate(Vector3.up * mouseX);
        }
    }
}
