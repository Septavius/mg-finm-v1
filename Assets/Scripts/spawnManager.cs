using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnManager : MonoBehaviour
{
    public List<GameObject> newGhostList = new List<GameObject>();
    static public int defineGhost;
    Vector3 spawnCoords;
    private System.Random random;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Spawning());
    }

    // Update is called once per frame
    private IEnumerator Spawning()
    {
        while (true)
        {
            if (Timer.roundStart == true)
            { 
                random = new System.Random();

                int randomIndex = random.Next(0, newGhostList.Count); //Inicializo un random y de ah� elijo a uno de los prefabs de fantamas.

                int randomSpawnPosition = random.Next(0, 2);

                Debug.Log(randomSpawnPosition);

                defineGhost = randomIndex; //Tomo el TIPO de fantasma usando el indice

                Debug.Log(defineGhost + " - Indice Lista");

                if (randomSpawnPosition == 0)
                {
                    spawnCoords = new Vector3(Random.Range(10, 95f), transform.position.y, Random.Range(5f, 10f)); //SPAWNEA DE UN LADO
                }
                else if (randomSpawnPosition == 1)
                {
                    spawnCoords = new Vector3(Random.Range(10, 95f), transform.position.y, Random.Range(75f, 95f)); //SPAWNEA DEL OTRO
                }

                GameObject newGhost = Instantiate(newGhostList[defineGhost], spawnCoords, Quaternion.identity);

                Enemy Enemydata = newGhost.GetComponent<Enemy>();

                Enemydata.ghostData.Initialize();
            }

            yield return new WaitForSeconds(5);

        }
    }
}
