using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class winOrDefeat : MonoBehaviour
{
    private int activeScene;
    public GameObject VictoryPanel;
    public GameObject pausePanel;
    static public bool isPaused = false;

    // Start is called before the first frame update

    private void Start()
    {
        activeScene = SceneManager.GetActiveScene().buildIndex;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!isPaused)
            {
                pauseGame();
            }
            else
            {
                ResumeGame();
            }
        }

        Victory();
    }

    public void pauseGame()
    {
        isPaused = true;
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0f;
        pausePanel.SetActive(true);
    }
    public void Retry()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = 1f;
        SceneManager.LoadSceneAsync(activeScene);
    }

    public void QuitMainMenu()
    {
        audioManager.instancia.pauseAudio("forestMusic");
        Time.timeScale = 1f;
        SceneManager.LoadSceneAsync(0);
    }

    public void ResumeGame()
    {
        isPaused = false;
        Cursor.lockState = CursorLockMode.Locked;
        pausePanel.SetActive(false);
        Time.timeScale = 1f;
    }

    public void Victory()
    {
        if (Timer.gameWon == true)
        {
            Cursor.lockState = CursorLockMode.None;
            VictoryPanel.SetActive(true);
            Time.timeScale = 0f;
        }
    }
}
