using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeMeter : MonoBehaviour
{

    [SerializeField] Slider volumeSlider;
    // Start is called before the first frame update
    void Start()
    {
        if (!PlayerPrefs.HasKey("volume"))
        {
            PlayerPrefs.SetFloat("volume", 1); //Si el usuario nunca modific� el volumen, va por defecto a 100%
            saveVolume();
        }
        else
        {
            loadVolume(); //Si tiene datos guardados, se cargan.
        }
    }

    // Update is called once per frame

    public void ChangeVolume()
    {
        AudioListener.volume = volumeSlider.value;
    }

    private void loadVolume()
    {
        volumeSlider.value = PlayerPrefs.GetFloat("volume");
    }

    private void saveVolume()
    {
        PlayerPrefs.SetFloat("volume", volumeSlider.value);
    }
}
