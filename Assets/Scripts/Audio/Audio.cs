using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Audio
{
    public string AudioName;

    public AudioClip clipAudio;

    [Range(0f, 1f)]
    public float Volume;

    [Range(0f, 1f)]
    public float Pitch;

    public bool loop;

    [HideInInspector]

    public AudioSource audioSource;
}
