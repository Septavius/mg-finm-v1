using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class audioManager : MonoBehaviour
{
    public Audio[] sfx;
    public static audioManager instancia;

    void Awake()
    {
        if (instancia == null)
        {
            instancia = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        foreach (Audio s in sfx)
        {
            s.audioSource = gameObject.AddComponent<AudioSource>();
            s.audioSource.clip = s.clipAudio;
            s.audioSource.volume = s.Volume;
            s.audioSource.pitch = s.Pitch;
            s.audioSource.loop = s.loop;
        }
    }

    public void playAudio(string name)
    {
        Audio s = Array.Find(sfx, sound => sound.AudioName == name);
        if (s == null)
        {
            Debug.LogWarning("Sonido: " + name + " no encontrado.");
            return;
        }
        else
        {
            s.audioSource.Play();
        }
    }

    public void pauseAudio(string name)
    {
        Audio s = Array.Find(sfx, sonido => sonido.AudioName == name);
        if (s == null)
        {
            Debug.LogWarning("Sonido: " + name + " no encontrado.");
            return;
        }
        else
        {
            s.audioSource.Pause();
        }
    }
}
