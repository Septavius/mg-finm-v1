using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using TMPro;

public class Enemy : MonoBehaviour
{
    public GhostInfo ghostData;
    private string ghostWord, ghostWord_2;
    private int damage;

    private bool shieldActive;
    private bool halfDoubleGhost;
    private int defineGhostType;

    public NavMeshAgent navi;
    private GameObject Movtarget;
    private GameObject player;

    public TextMeshPro ghostText;
    public TextMeshPro ghostText_2;
    // Start is called before the first frame update
    void Start()
    {
        defineGhostType = spawnManager.defineGhost; //Tomo el TIPO de fantasma usando el indice
        Debug.Log("Tipo de fantasma: " + defineGhostType);

        Movtarget = GameObject.Find("Pillar");
        player = GameObject.Find("Player");

        damage = ghostData.damage;
        Debug.Log("Da�o fantasma: " + damage);

        if (defineGhostType == 0)
        {
            ghostWord = ghostData.ghostWord;
            ghostText.text = ghostWord;
        }

        else if (defineGhostType == 1)
        {
            ghostWord = ghostData.ghostWord;
            ghostText.text = ghostWord;
            ghostWord_2 = ghostData.ghostWord_2;

            shieldActive = true;
            Debug.Log("Estado escudo: " + shieldActive);
        }
        else if (defineGhostType == 2)
        {
            ghostWord = ghostData.ghostWord;
            ghostText.text = ghostWord;
            ghostWord_2 = ghostData.ghostWord_2;
            ghostText_2.text = ghostWord_2;

            halfDoubleGhost = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        checkDeath();
        textRotation();
    }

    private void LateUpdate()
    {
        Movement();
    }

    void checkDeath()
    {
        if (defineGhostType == 0) //COND. MUERTE FANTASMA BASICO
        {
            if (ghostWord == PlayerType.playerInput)
            {
                Destroy(this.gameObject);
            }
        }
        else if (defineGhostType == 1) //COND. MUERTE FANTASMA CON ESCUDO
        {

            if (PlayerType.playerInput == ghostWord_2) //Si el jugador escribe escudo:
            {
                shieldActive = false;
                PlayerType.playerInput = "";
                Debug.Log("Estado escudo: " + shieldActive);

                Transform forcefield = transform.Find("forceField");

                Destroy(forcefield.gameObject); //destruye el escudo
            }

            if (shieldActive == false)
            {
                if (ghostWord == PlayerType.playerInput)
                {
                    Destroy(this.gameObject);
                }
            }
        }
        else if (defineGhostType == 2) //COND. MUERTE FANTASMA DOBLE PALABRA.
        {
            if (halfDoubleGhost == false) //Si todavia no se escribi� ninguna de sus dos palabras, entonces indiferente:
            {
                if ((PlayerType.playerInput == ghostWord) || (PlayerType.playerInput == ghostWord_2))
                {
                    halfDoubleGhost = true;

                    if(PlayerType.playerInput == ghostWord) //Se elimina la palabra escrita y queda la otra
                    {
                        Destroy(ghostText);
                    }
                    else if (PlayerType.playerInput == ghostWord_2)
                    {
                        Destroy(ghostText_2);
                    }
                }
            }
            else //Si ya se escribi� una palabra entonces:
            {
                if (ghostText == null)
                {
                    if (PlayerType.playerInput == ghostWord_2)
                    {
                        Destroy(this.gameObject);
                    }
                }
                else if (ghostText_2 == null) //Si se escribe la palabra que falta, se elimina
                {
                    if (PlayerType.playerInput == ghostWord)
                    {
                        Destroy(this.gameObject);
                    }
                }
            }
        }

        if (Timer.roundStart == false)
        {
            Destroy(this.gameObject);
        }
    }

    void Movement()
    {
        navi.SetDestination(Movtarget.transform.position);
    }

    void textRotation()
    {
        Vector3 rotationDirection = player.transform.position - transform.position;

        Quaternion textRotation = Quaternion.LookRotation(rotationDirection, Vector3.up);

        if (defineGhostType == 2)
        {
            ghostText.transform.rotation = textRotation;

            ghostText.transform.Rotate(0, 180, 0);

            ghostText_2.transform.rotation = textRotation;

            ghostText_2.transform.Rotate(0, 180, 0);
        }
        else
        {
            ghostText.transform.rotation = textRotation;

            ghostText.transform.Rotate(0, 180, 0);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("DefendTarget"))
        {
            pillarScript GOScript = collision.gameObject.GetComponent<pillarScript>();
            Debug.Log(GOScript);
            GOScript.takeDamage(damage);

            Destroy(this.gameObject);
        }
    }
}
