using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI timerText;
    [SerializeField] TextMeshProUGUI roundText;

    [SerializeField] float intermissionTime, roundTime;

    static public bool roundStart = false;
    static public bool gameWon = false;
    bool musicPlaying = false;
    private int roundCount = 0;

    void Start()
    {
        gameWon = false;
        roundCount = 0;
        roundStart = false;
        intermissionTime = 15;
        roundTime = 120;
    }

    // Update is called once per frame
    void Update()
    {
        roundTimer();
        winCondition();
    }

    private void roundTimer()
    {
        if (roundStart == false) //si estamos en tiempo de gracia
        {
            if (intermissionTime > 0)
            {
                intermissionTime -= Time.deltaTime;

            }

            else if (intermissionTime <= 0)
            {
                intermissionTime = 0;

                roundTime = 120;

                roundStart = true;

                roundCount += 1;

                roundText.text ="Ronda " + roundCount + "/3";

                Debug.Log("Estado de ronda: " + roundStart);
            }

            int minutes = Mathf.FloorToInt(intermissionTime / 60);
            int seconds = Mathf.FloorToInt(intermissionTime % 60);

            timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
        }

        else if (roundStart) //si estamos en tiempo de ronda
        {
            if (roundTime > 0)
            {
                roundTime -= Time.deltaTime;

                if (musicPlaying == false)
                {
                    audioManager.instancia.playAudio("forestMusic");
                    musicPlaying = true;
                }
            }

            else if (roundTime <= 0)
            {

                roundTime = 0;

                intermissionTime = 15;

                roundStart = false;

                Debug.Log("Estado de ronda: " + roundStart);

                audioManager.instancia.pauseAudio("forestMusic");

                musicPlaying = false;
            }

            int minutes = Mathf.FloorToInt(roundTime / 60);
            int seconds = Mathf.FloorToInt(roundTime % 60);

            timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
        }
    }

    private void winCondition()
    {
        if(roundCount > 3)
        {
            Debug.Log("JUEGO GANADO"); 
            gameWon = true;
        }
    }
}
