using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pillarScript : MonoBehaviour
{

    [SerializeField] public Slider healthBar;

    [SerializeField] public GameObject defeatPanel;

    static public bool isDefeated = false;

    float maxValue, currentValue;

    private void Start()
    {
        isDefeated = false;
        maxValue = 100;
        currentValue = maxValue;
    }

    private void Update()
    {
        if (currentValue <= 0)
        {
            isDefeated = true;
            Cursor.lockState = CursorLockMode.None;
            Time.timeScale = 0f;
            defeatPanel.SetActive(true);
        }
    }
    void HealthBarUpdate(float currentValue, float maxValue)
    {
        healthBar.value = currentValue / maxValue; //act valor slider
    }

    public void takeDamage(float damage)
    {
        currentValue -= damage;
        Debug.Log(currentValue);
        HealthBarUpdate(currentValue, maxValue);
    }

}
